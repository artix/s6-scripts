SYSCONFDIR ?= /etc
S6DIR = $(SYSCONFDIR)/s6
SVDIR = $(S6DIR)/sv
ADMINSVDIR = $(S6DIR)/adminsv
FALLBACKSVDIR = $(S6DIR)/fallbacksv

PREFIX = /usr

MANDIR = $(PREFIX)/share/man/man8
SYSCTLCONFDIR = $(PREFIX)/lib/sysctl.d

LOGRELOAD = log-service-reload.sh
MODLOAD = modules-load
MAN = modules-load.8
RC = rc.local
SYSCTLCONF = 50-default.conf

CONFIGDIR = $(S6DIR)/config

CONFIG = \
	convfile \
	dmesg.conf \
	hwclock.conf \
	mount-tmpfs.conf \
	network-detection.conf \
	ttyS.conf \
	udevd.conf

BINFMTDIR = binfmt
BOOTDIR = boot
CLEANUPDIR = cleanup
CSDIR = console-setup
DEFAULTDIR = default
DMESGLOGDIR = dmesg-log
DMESGSRVDIR = dmesg-srv
GETTYDIR = getty
HOSTNAMEDIR = hostname
HWCLOCKDIR = hwclock
KMODDIR = kmod-static-nodes
LOCALEDIR = locale
LOGINDDIR = logind
MISCDIR = misc
MODULESDIR = modules
MOUNTDIR = mount
MCGDIR = mount-cgroups
MDEVFSDIR = mount-devfs
MFSDIR = mount-filesystems
MNETDIR = mount-net
MPROCDIR = mount-procfs
MSYSFSDIR = mount-sysfs
MTMPFSDIR = mount-tmpfs
NETLODIR = net-lo
NETWODIR = network
NETWODETECTDIR = network-detection
RNDSEEDDIR = random-seed
RCLOCALDIR = rc-local
REMNTROOTDIR = remount-root
SETUPDIR = setup
SWAPDIR = swap
SYSCTLDIR = sysctl
SYSUSERSDIR = sysusers
TMPFILESDEVDIR = tmpfiles-dev
TMPFILESSETUPDIR = tmpfiles-setup
TTYDIRS = tty1 tty2 tty3 tty4 tty5 tty6
TTYSDIR = ttyS
UDEVDIR = udev
UDEVADMDIR = udevadm
UDEVLOGDIR = udevd-log
UDEVSRVDIR = udevd-srv

BINFMT = $(subst binfmt/dependencies.d,, $(wildcard binfmt/*))
BINFMTDEPS = $(wildcard binfmt/*/*)
BOOT = $(subst boot/contents.d,, $(wildcard boot/*))
BOOTCONTENTS = $(wildcard boot/*/*)
CLEANUP = $(subst cleanup/dependencies.d,, $(wildcard cleanup/*))
CLEANUPDEPS = $(wildcard cleanup/*/*)
CS = $(subst console-setup/dependencies.d,, $(wildcard console-setup/*))
CSDEPS = $(wildcard console-setup/*/*)
DEFAULT = $(subst default/contents.d,, $(wildcard default/*))
DEFAULTCONTENTS = $(wildcard default/*/*)
DMESGLOG = $(wildcard dmesg-log/*)
DMESGSRV = $(subst dmesg-srv/dependencies.d,, $(wildcard dmesg-srv/*))
DMESGSRVDEPS = $(wildcard dmesg-srv/*/*)
GETTY = $(subst getty/contents.d,, $(wildcard getty/*))
GETTYCONTENTS = $(wildcard getty/*/*)
HOSTNAME = $(subst hostname/dependencies.d,, $(wildcard hostname/*))
HOSTNAMEDEPS = $(wildcard hostname/*/*)
HWCLOCK = $(wildcard hwclock/*)
KMOD = $(subst kmod-static-nodes/dependencies.d,, $(wildcard kmod-static-nodes/*))
KMODDEPS = $(wildcard kmod-static-nodes/*/*)
LOCALE = $(subst locale/dependencies.d,, $(wildcard locale/*))
LOCALEDEPS = $(wildcard locale/*/*)
LOGIND = $(wildcard logind/*)
MISC = $(subst misc/contents.d,, $(wildcard misc/*))
MISCCONTENTS = $(wildcard misc/*/*)
MODULES = $(subst modules/dependencies.d,, $(wildcard modules/*))
MODULESDEPS = $(wildcard modules/*/*)
MOUNT = $(subst mount/contents.d,, $(wildcard mount/*))
MOUNTCONTENTS = $(wildcard mount/*/*)
MCG = $(subst mount-cgroups/dependencies.d,, $(wildcard mount-cgroups/*))
MCGDEPS = $(wildcard mount-cgroups/*/*)
MDEVFS = $(subst mount-devfs/dependencies.d,, $(wildcard mount-devfs/*))
MDEVFSDEPS = $(wildcard mount-devfs/*/*)
MFS = $(subst mount-filesystems/dependencies.d,, $(wildcard mount-filesystems/*))
MFSDEPS = $(wildcard mount-filesystems/*/*)
MNET = $(subst mount-net/dependencies.d,, $(wildcard mount-net/*))
MNETDEPS = $(wildcard mount-net/*/*)
MPROC = $(wildcard mount-procfs/*)
MSYSFS = $(subst mount-sysfs/dependencies.d,, $(wildcard mount-sysfs/*))
MSYSFSDEPS = $(wildcard mount-sysfs/*/*)
MTMPFS = $(wildcard mount-tmpfs/*)
NETLO = $(wildcard net-lo/*)
NETWO = $(subst network/contents.d,, $(wildcard network/*))
NETWOCONTENTS = $(wildcard network/*/*)
NETWODETECT = $(subst network-detection/dependencies.d,, $(wildcard network-detection/*))
NETWODETECTDEPS = $(wildcard network-detection/*/*)
RNDSEED = $(subst random-seed/dependencies.d,, $(wildcard random-seed/*))
RNDSEEDDEPS = $(wildcard random-seed/*/*)
RCLOCAL = $(subst rc-local/dependencies.d,, $(wildcard rc-local/*))
REMNTROOT = $(subst remount-root/dependencies.d,, $(wildcard remount-root/*))
REMNTROOTDEPS = $(wildcard remount-root/*/*)
SETUP = $(subst setup/contents.d,, $(wildcard setup/*))
SETUPCONTENTS = $(wildcard setup/*/*)
SWAP = $(subst swap/dependencies.d,, $(wildcard swap/*))
SWAPDEPS = $(wildcard swap/*/*)
SYSCTL = $(wildcard sysctl/*)
SYSUSERS = $(subst sysusers/dependencies.d,, $(wildcard sysusers/*))
SYSUSERSDEPS = $(wildcard sysusers/*/*)
TMPFILESDEV = $(subst tmpfiles-dev/dependencies.d,, $(wildcard tmpfiles-dev/*))
TMPFILESDEVDEPS = $(wildcard tmpfiles-dev/*/*)
TMPFILESSETUP = $(subst tmpfiles-setup/dependencies.d,, $(wildcard tmpfiles-setup/*))
TMPFILESSETUPDEPS = $(wildcard tmpfiles-setup/*/*)
TTY = $(subst tty/dependencies.d,, $(wildcard tty/*))
TTYDEPS = $(wildcard tty/*/*)
TTYS = $(subst ttyS/dependencies.d,, $(wildcard ttyS/*))
TTYSDEPS = $(wildcard ttyS/*/*)
UDEV = $(subst udev/contents.d,, $(wildcard udev/*))
UDEVCONTENTS = $(wildcard udev/*/*)
UDEVADM = $(subst udevadm/dependencies.d,, $(wildcard udevadm/*))
UDEVADMDEPS = $(wildcard udevadm/*/*)
UDEVLOG = $(wildcard udevd-log/*)
UDEVSRV = $(subst udevd-srv/dependencies.d,, $(wildcard udevd-srv/*))
UDEVSRVDEPS = $(wildcard udevd-srv/*/*)

DIRMODE = -dm0755
MODE = -m0644
EMODE = -m0755

install_s6:
	install $(DIRMODE) $(DESTDIR)$(SVDIR)
	install $(DIRMODE) $(DESTDIR)$(CONFIGDIR)

	install $(MODE) $(CONFIG) $(DESTDIR)$(CONFIGDIR)

	for dir in $(TTYDIRS); do \
		install $(MODE) tty.conf $(DESTDIR)$(CONFIGDIR)/$$dir.conf; \
	done;

	install $(MODE) $(RC) $(DESTDIR)$(S6DIR)

	install $(MODE) $(LOGRELOAD) $(DESTDIR)$(S6DIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(BINFMTDIR)/dependencies.d
	install $(MODE) $(BINFMT) $(DESTDIR)$(SVDIR)/$(BINFMTDIR)
	install $(MODE) $(BINFMTDEPS) $(DESTDIR)$(SVDIR)/$(BINFMTDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(BOOTDIR)/contents.d
	install $(MODE) $(BOOT) $(DESTDIR)$(SVDIR)/$(BOOTDIR)
	install $(MODE) $(BOOTCONTENTS) $(DESTDIR)$(SVDIR)/$(BOOTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(CLEANUPDIR)/dependencies.d
	install $(MODE) $(CLEANUP) $(DESTDIR)$(SVDIR)/$(CLEANUPDIR)
	install $(MODE) $(CLEANUPDEPS) $(DESTDIR)$(SVDIR)/$(CLEANUPDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(CSDIR)/dependencies.d
	install $(MODE) $(CS) $(DESTDIR)$(SVDIR)/$(CSDIR)
	install $(MODE) $(CSDEPS) $(DESTDIR)$(SVDIR)/$(CSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(ADMINSVDIR)/$(DEFAULTDIR)/contents.d
	install $(MODE) $(DEFAULT) $(DESTDIR)$(ADMINSVDIR)/$(DEFAULTDIR)
	install $(MODE) $(DEFAULTCONTENTS) $(DESTDIR)$(ADMINSVDIR)/$(DEFAULTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(FALLBACKSVDIR)/$(DEFAULTDIR)/contents.d
	install $(MODE) $(DEFAULT) $(DESTDIR)$(FALLBACKSVDIR)/$(DEFAULTDIR)
	install $(MODE) $(DEFAULTCONTENTS) $(DESTDIR)$(FALLBACKSVDIR)/$(DEFAULTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(DMESGLOGDIR)
	install $(MODE) $(DMESGLOG) $(DESTDIR)$(SVDIR)/$(DMESGLOGDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(DMESGSRVDIR)/dependencies.d
	install $(MODE) $(DMESGSRV) $(DESTDIR)$(SVDIR)/$(DMESGSRVDIR)
	install $(MODE) $(DMESGSRVDEPS) $(DESTDIR)$(SVDIR)/$(DMESGSRVDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(GETTYDIR)/contents.d
	install $(MODE) $(GETTY) $(DESTDIR)$(SVDIR)/$(GETTYDIR)
	install $(MODE) $(GETTYCONTENTS) $(DESTDIR)$(SVDIR)/$(GETTYDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(HOSTNAMEDIR)/dependencies.d
	install $(MODE) $(HOSTNAME) $(DESTDIR)$(SVDIR)/$(HOSTNAMEDIR)
	install $(MODE) $(HOSTNAMEDEPS) $(DESTDIR)$(SVDIR)/$(HOSTNAMEDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(HWCLOCKDIR)
	install $(MODE) $(HWCLOCK) $(DESTDIR)$(SVDIR)/$(HWCLOCKDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(KMODDIR)/dependencies.d
	install $(MODE) $(KMOD) $(DESTDIR)$(SVDIR)/$(KMODDIR)
	install $(MODE) $(KMODDEPS) $(DESTDIR)$(SVDIR)/$(KMODDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(LOCALEDIR)/dependencies.d
	install $(MODE) $(LOCALE) $(DESTDIR)$(SVDIR)/$(LOCALEDIR)
	install $(MODE) $(LOCALEDEPS) $(DESTDIR)$(SVDIR)/$(LOCALEDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(LOGINDDIR)/contents.d
	install $(MODE) $(LOGIND) $(DESTDIR)$(SVDIR)/$(LOGINDDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MISCDIR)/contents.d
	install $(MODE) $(MISC) $(DESTDIR)$(SVDIR)/$(MISCDIR)
	install $(MODE) $(MISCCONTENTS) $(DESTDIR)$(SVDIR)/$(MISCDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MODULESDIR)/dependencies.d
	install $(MODE) $(MODULES) $(DESTDIR)$(SVDIR)/$(MODULESDIR)
	install $(MODE) $(MODULESDEPS) $(DESTDIR)$(SVDIR)/$(MODULESDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(PREFIX)/bin
	install $(EMODE) $(MODLOAD) $(DESTDIR)$(PREFIX)/bin

	install $(DIRMODE) $(DESTDIR)$(MANDIR)
	install $(MODE) $(MAN) $(DESTDIR)$(MANDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MOUNTDIR)/contents.d
	install $(MODE) $(MOUNT) $(DESTDIR)$(SVDIR)/$(MOUNTDIR)
	install $(MODE) $(MOUNTCONTENTS) $(DESTDIR)$(SVDIR)/$(MOUNTDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MCGDIR)/dependencies.d
	install $(MODE) $(MCG) $(DESTDIR)$(SVDIR)/$(MCGDIR)
	install $(MODE) $(MCGDEPS) $(DESTDIR)$(SVDIR)/$(MCGDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MDEVFSDIR)/dependencies.d
	install $(MODE) $(MDEVFS) $(DESTDIR)$(SVDIR)/$(MDEVFSDIR)
	install $(MODE) $(MDEVFSDEPS) $(DESTDIR)$(SVDIR)/$(MDEVFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(ADMINSVDIR)/$(MFSDIR)/dependencies.d
	install $(MODE) $(MFS) $(DESTDIR)$(ADMINSVDIR)/$(MFSDIR)
	install $(MODE) $(MFSDEPS) $(DESTDIR)$(ADMINSVDIR)/$(MFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(FALLBACKSVDIR)/$(MFSDIR)/dependencies.d
	install $(MODE) $(MFS) $(DESTDIR)$(FALLBACKSVDIR)/$(MFSDIR)
	install $(MODE) $(MFSDEPS) $(DESTDIR)$(FALLBACKSVDIR)/$(MFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MNETDIR)/dependencies.d
	install $(MODE) $(MNET) $(DESTDIR)$(SVDIR)/$(MNETDIR)
	install $(MODE) $(MNETDEPS) $(DESTDIR)$(SVDIR)/$(MNETDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MPROCDIR)
	install $(MODE) $(MPROC) $(DESTDIR)$(SVDIR)/$(MPROCDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MSYSFSDIR)/dependencies.d
	install $(MODE) $(MSYSFS) $(DESTDIR)$(SVDIR)/$(MSYSFSDIR)
	install $(MODE) $(MSYSFSDEPS) $(DESTDIR)$(SVDIR)/$(MSYSFSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(MTMPFSDIR)
	install $(MODE) $(MTMPFS) $(DESTDIR)$(SVDIR)/$(MTMPFSDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(NETLODIR)
	install $(MODE) $(NETLO) $(DESTDIR)$(SVDIR)/$(NETLODIR)

	install $(DIRMODE) $(DESTDIR)$(ADMINSVDIR)/$(NETWODIR)/contents.d
	install $(MODE) $(NETWO) $(DESTDIR)$(ADMINSVDIR)/$(NETWODIR)
	install $(MODE) $(NETWOCONTENTS) $(DESTDIR)$(ADMINSVDIR)/$(NETWODIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(FALLBACKSVDIR)/$(NETWODIR)/contents.d
	install $(MODE) $(NETWO) $(DESTDIR)$(FALLBACKSVDIR)/$(NETWODIR)
	install $(MODE) $(NETWOCONTENTS) $(DESTDIR)$(FALLBACKSVDIR)/$(NETWODIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(NETWODETECTDIR)/dependencies.d
	install $(MODE) $(NETWODETECT) $(DESTDIR)$(SVDIR)/$(NETWODETECTDIR)
	install $(MODE) $(NETWODETECTDEPS) $(DESTDIR)$(SVDIR)/$(NETWODETECTDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(RNDSEEDDIR)/dependencies.d
	install $(MODE) $(RNDSEED) $(DESTDIR)$(SVDIR)/$(RNDSEEDDIR)
	install $(MODE) $(RNDSEEDDEPS) $(DESTDIR)$(SVDIR)/$(RNDSEEDDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(ADMINSVDIR)/$(RCLOCALDIR)/dependencies.d
	install $(MODE) $(RCLOCAL) $(DESTDIR)$(ADMINSVDIR)/$(RCLOCALDIR)

	install $(DIRMODE) $(DESTDIR)$(FALLBACKSVDIR)/$(RCLOCALDIR)/dependencies.d
	install $(MODE) $(RCLOCAL) $(DESTDIR)$(FALLBACKSVDIR)/$(RCLOCALDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(REMNTROOTDIR)/dependencies.d
	install $(MODE) $(REMNTROOT) $(DESTDIR)$(SVDIR)/$(REMNTROOTDIR)
	install $(MODE) $(REMNTROOTDEPS) $(DESTDIR)$(SVDIR)/$(REMNTROOTDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SETUPDIR)/contents.d
	install $(MODE) $(SETUP) $(DESTDIR)$(SVDIR)/$(SETUPDIR)
	install $(MODE) $(SETUPCONTENTS) $(DESTDIR)$(SVDIR)/$(SETUPDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SWAPDIR)/dependencies.d
	install $(MODE) $(SWAP) $(DESTDIR)$(SVDIR)/$(SWAPDIR)
	install $(MODE) $(SWAPDEPS) $(DESTDIR)$(SVDIR)/$(SWAPDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SYSCTLDIR)
	install $(MODE) $(SYSCTL) $(DESTDIR)$(SVDIR)/$(SYSCTLDIR)

	install $(DIRMODE) $(DESTDIR)$(SYSCTLCONFDIR)
	install $(MODE) $(SYSCTLCONF) $(DESTDIR)$(SYSCTLCONFDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(SYSUSERSDIR)/dependencies.d
	install $(MODE) $(SYSUSERS) $(DESTDIR)$(SVDIR)/$(SYSUSERSDIR)
	install $(MODE) $(SYSUSERSDEPS) $(DESTDIR)$(SVDIR)/$(SYSUSERSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TMPFILESDEVDIR)/dependencies.d
	install $(MODE) $(TMPFILESDEV) $(DESTDIR)$(SVDIR)/$(TMPFILESDEVDIR)
	install $(MODE) $(TMPFILESDEVDEPS) $(DESTDIR)$(SVDIR)/$(TMPFILESDEVDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TMPFILESSETUPDIR)/dependencies.d
	install $(MODE) $(TMPFILESSETUP) $(DESTDIR)$(SVDIR)/$(TMPFILESSETUPDIR)
	install $(MODE) $(TMPFILESSETUPDEPS) $(DESTDIR)$(SVDIR)/$(TMPFILESSETUPDIR)/dependencies.d

	for dir in $(TTYDIRS); do \
		install $(DIRMODE) $(DESTDIR)$(SVDIR)/$$dir/dependencies.d; \
		install $(MODE) $(TTY) $(DESTDIR)$(SVDIR)/$$dir; \
		install $(MODE) $(TTYDEPS) $(DESTDIR)$(SVDIR)/$$dir/dependencies.d; \
		sed -i "s/TTYDIR/$$dir/" $(DESTDIR)$(SVDIR)/$$dir/run; \
	done;

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(TTYSDIR)/dependencies.d
	install $(MODE) $(TTYS) $(DESTDIR)$(SVDIR)/$(TTYSDIR)
	install $(MODE) $(TTYSDEPS) $(DESTDIR)$(SVDIR)/$(TTYSDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVDIR)/contents.d
	install $(MODE) $(UDEV) $(DESTDIR)$(SVDIR)/$(UDEVDIR)
	install $(MODE) $(UDEVCONTENTS) $(DESTDIR)$(SVDIR)/$(UDEVDIR)/contents.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVADMDIR)/dependencies.d
	install $(MODE) $(UDEVADM) $(DESTDIR)$(SVDIR)/$(UDEVADMDIR)
	install $(MODE) $(UDEVADMDEPS) $(DESTDIR)$(SVDIR)/$(UDEVADMDIR)/dependencies.d

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVLOGDIR)
	install $(MODE) $(UDEVLOG) $(DESTDIR)$(SVDIR)/$(UDEVLOGDIR)

	install $(DIRMODE) $(DESTDIR)$(SVDIR)/$(UDEVSRVDIR)/dependencies.d
	install $(MODE) $(UDEVSRV) $(DESTDIR)$(SVDIR)/$(UDEVSRVDIR)
	install $(MODE) $(UDEVSRVDEPS) $(DESTDIR)$(SVDIR)/$(UDEVSRVDIR)/dependencies.d

install: install_s6
