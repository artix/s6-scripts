# s6-scripts
A collection of essential s6-rc oneshots and longruns for startup/shutdown. Much credit goes to konimex's [runit-rc](https://gitea.artixlinux.org/artix/runit-rc) and obarun's [s6-boot](https://libraries.io/github/Obarun/s6-boot) for making this possible.
